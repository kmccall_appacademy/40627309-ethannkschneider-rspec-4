class Temperature

  def initialize(options = {})
    default = { f: nil, c: nil }
    options = default.merge(options)
    @fahrenheit, @celsius = options[:f], options[:c]
  end

  attr_reader :fahrenheit, :celcius

  def in_fahrenheit
    if @fahrenheit
      return @fahrenheit
    else
      return (9.0/5) * @celsius + 32
    end
  end

  def in_celsius
    if @celsius
      return @celsius
    else
      return (5/9.0) * (@fahrenheit - 32)
    end
  end

  def self.from_celsius(temp)
    Temperature.new({ c: temp })
  end

  def self.from_fahrenheit(temp)
    Temperature.new({ f: temp })
  end
end

class Celsius < Temperature
  def initialize(celsius)
    @celsius = celsius
  end

end

class Fahrenheit < Temperature
  def initialize(fahrenheit)
    @fahrenheit = fahrenheit
  end
end
