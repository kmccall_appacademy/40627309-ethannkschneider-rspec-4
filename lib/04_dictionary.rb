class Dictionary
  def initialize
    @entries = {}
  end

  attr_reader :entries

  def add(entry)
     if entry.is_a?(String)
       @entries[entry] = nil
     else
       @entries.merge!(entry)
     end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(word)
    keywords.include?(word)
  end

  def find(str)
    keys = keywords.select { |key| key.include?(str) }
    found = @entries.select { |key, val| keys.include?(key) }
    found
  end

  def printable
    printable_entries = ""
    keywords.each_with_index do |word, index|
      unless index == keywords.length - 1
        printable_entries += "[#{word}] \"#{@entries[word]}\"\n"
      else
        printable_entries += "[#{word}] \"#{@entries[word]}\""
      end
    end
    printable_entries
  end
end
