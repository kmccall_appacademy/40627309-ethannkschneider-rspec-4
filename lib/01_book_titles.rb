class Book
  CONJUNCTIONS = ["and", "but", "or", "nor", "except", "also"]
  PREPOSITIONS = ["in", "of", "at", "to", "on", "over", "above", "under"]
  ARTICLES = ["a", "an", "the"]

  def initialize
    @title = ""
  end

  attr_reader :title

  def title=(book_title)
    words = book_title.split
    result = []
    # first, capitalize the first letter of every word
    words.map!(&:capitalize)
    # next, uncapitalize prepositions, conjunctions, articles, except the first word
    words.map! do |word|
      if conjunction?(word) || preposition?(word) || article?(word)
        word.downcase
      else
        word
      end
    end

    # finally, capitalize "I" always
    words.map! do |word|
      if word == "i" || word == "I"
        word.upcase
      else
        word
      end
    end

    str = words.join(" ")
    @title = str[0].upcase + str[1..-1]

  end

  def article?(word)
    ARTICLES.include?(word.downcase)
  end

  def preposition?(word)
    PREPOSITIONS.include?(word.downcase)
  end

  def conjunction?(word)
    CONJUNCTIONS.include?(word.downcase)
  end

  def capitalize(word)
    if singleton?(word)
      return word.upcase
    else
      return word[0].upcase + word[1..-1].downcase
    end
  end

  def singleton?(word)
    word.length == 1
  end
end
