class Timer

  def initialize
    @seconds = 0
  end

  attr_accessor :seconds

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds - (hours * 3600)) / 60
    seconds =  (@seconds - (hours * 3600) - (minutes * 60))

    hours_str = ""
    minutes_str = ""
    seconds_str = ""

    hours.to_s.length == 1 ? hours_str = "0#{hours}" : hours_str = hours.to_s
    minutes.to_s.length == 1 ? minutes_str = "0#{minutes}" : minutes_str = minutes.to_s
    seconds.to_s.length == 1 ? seconds_str = "0#{seconds}" : seconds_str = seconds.to_s

    "#{hours_str}:#{minutes_str}:#{seconds_str}"
  end
end
