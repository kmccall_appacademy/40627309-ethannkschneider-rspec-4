class Friend
  def greeting(friend = nil)
    return "Hello!" unless friend
    return "Hello, #{friend}!" if friend 
  end
end
